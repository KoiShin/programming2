#! /usr/bin/ruby -Ku
# -*- coding: utf-8 -*-

require 'webrick'

config = {
  :Port => 50915,
  :DocumentRoot => '.'
}

server = WEBrick::HTTPServer.new(config)

trap(:INT) do
  server.shutdown
end

server.start
